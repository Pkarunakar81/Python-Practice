#Working with External Libraries


import math

print("It's math! It has type {}".format(type(math)))

##It's math! It has type <class 'module'>
We can see all the names in math using the built-in function dir().

print(dir(math))

print("pi to 4 significant digits = {:.4}".format(math.pi))
help(math.log)


Other import syntax

import math as mt
mt.pi
import math
mt = math

#we could refer to all the variables in the math module by themselves
from math import *
print(pi, log(32, 2))

from math import *
from numpy import *
print(pi, log(32, 2))
The problem in this case is that the math and numpy modules both have functions called log, 
but they have different semantics. 
Because we import from numpy second, its log overwrites (or "shadows") the log variable 
we imported from math.

A good compromise is to import only the specific things we'll need from each module:
    


#Submodules¶


import numpy
print("numpy.random is a", type(numpy.random))
print("it contains names such as...",
      dir(numpy.random)[-15:]
     )
     
So if we import numpy as above, then calling a function in the random "submodule" will require two dots.

# Roll 10 dice
rolls = numpy.random.randint(low=1, high=6, size=10)
rolls


1: type() (what is this thing?)

type(rolls)
numpy.ndarray


2: dir() (what can I do with it?)

print(dir(rolls))


3: help() (tell me more)

# That "ravel" attribute sounds interesting. I'm a big classical music fan.
help(rolls.ravel)

#Operator overloading¶
[3, 4, 1, 2, 2, 1] + 10
TypeError: can only concatenate list (not "int") to list
rolls + 10












