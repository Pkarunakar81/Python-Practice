
import math


print("It's math! It has type {}".format(type(math)))
#We can see all the names in math using the built-in function dir().
print(dir(math))
#We can access these variables using dot syntax
print("pi to 4 significant digits = {:.6}".format(math.log(2)))

#if we don't know what math.log does, we can call help() on it:

help(math.log)


#Other import syntax
#we can import it under a shorter alias to save some typing (though in this case "math" is already pretty short).

import math as mt
mt.pi


#using the below we can refer to all the variables in the math module by themselves

from math import *
print(pi, log(32, 2))





