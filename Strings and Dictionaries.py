

#String syntax¶
'''
x = 'Pluto is a planet'
y = "Pluto is a planet"
x == y '''

#Double quotes are convenient if your string contains a single quote character (e.g. representing an apostrophe).
#Similarly, it's easy to create a string that contains double-quotes if you wrap it in single quotes:

print("Pluto's a planet!")
print('My dog is named "Pluto"')

#print('Pluto's a planet!') #SyntaxError: invalid syntax
#We can fix this by "escaping" the single quote with a backslash.

print('Pluto\'s a planet!')
''' The table below summarizes some important uses of the backslash character.

What you type...	What you get	
\'	
'	
example : - 'What\'s up?'	
print(example) : - What's up?
\"	
"	
example : - "That's \"cool\""	
print(example) : - That's "cool"
\\	
\	
example : - "Look, a mountain: /\\"	
print(example) : - Look, a mountain: /\
\n	

example : - "1\n2 3"	
print(example) : - 
1
2 3'''
#If you want to add a space between the words, you can set end=' ' for the first print:
print("hello", end=' ')
print("pluto", end='')
#Or, if you want to ensure a newline at the end of the output, you can add it explicitly:

print("hello", end='')
print("pluto", end='\n')

#Strings are sequences¶

# Indexing
planet = 'Pluto'
planet[0]

#Result : -  'P'

# Slicing
planet[-3:] # result 'uto'
# Yes, we can even loop over them
[char+'! ' for char in planet]  #['P! ', 'l! ', 'u! ', 't! ', 'o! ']

planet[0] = 'B'#TypeError: 'str' object does not support item assignment
#String methods¶
# ALL CAPS
claim = "Pluto is a planet!"
claim.upper()
# all lowercase
claim.lower()
# Searching for the first index of a substring
claim.index('plan')
claim.startswith(planet)
# false because of missing exclamation mark
claim.endswith('planet')
words = claim.split()
words#result ['Pluto', 'is', 'a', 'planet!']
datestr = '1956-01-31'
year, month, day = datestr.split('-')
'/'.join([month, day, year])
' 👏 '.join([word.upper() for word in words])

#str.format()

"{}, you'll always be the {}th planet to me.".format(planet, position)

pluto_mass = 1.303 * 10**22
earth_mass = 5.9722 * 10**24
population = 52910390
#         2 decimal points   3 decimal points, format as percent     separate with commas
"{} weighs about {:.2} kilograms ({:.3%} of Earth's mass). It is home to {:,} Plutonians.".format(
    planet, pluto_mass, pluto_mass / earth_mass, population,
)
"Pluto weighs about 1.3e+22 kilograms (0.218% of Earth's mass). It is home to 52,910,390 Plutonians."


# Referring to format() arguments by index, starting from 0
s = """Pluto's a {0}.
No, it's a {1}.
{0}!
{1}!""".format('planet', 'dwarf planet')
print(s)
Pluto's a planet.
No, it's a dwarf planet.
planet!
dwarf planet!



#Dictionaries¶

Dictionaries are a built-in Python data structure for mapping keys to values.

numbers = {'one':1, 'two':2, 'three':3}

numbers['one']

1

We can use the same syntax to add another key, value pair

numbers['eleven'] = 11
numbers
result {'one': 1, 'two': 2, 'three': 3, 'eleven': 11}

#to change the value associated with an existing key

numbers['one'] = 'Pluto'
numbers

#result :  -{'one': 'Pluto', 'two': 2, 'three': 3, 'eleven': 11}

#dictionary comprehensions with a syntax similar to the list comprehensions

planets = ['Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn', 'Uranus', 'Neptune']
planet_to_initial = {planet: planet[0] for planet in planets}
planet_to_initial
{'Mercury': 'M',
 'Venus': 'V',
 'Earth': 'E',
 'Mars': 'M',
 'Jupiter': 'J',
 'Saturn': 'S',
 'Uranus': 'U',
 'Neptune': 'N'}
#the in operator tells us whether something is a key in the dictionary

'Saturn' in planet_to_initial

result True

A for loop over a dictionary will loop over its keys

for k in numbers:
    print("{} = {}".format(k, numbers[k]))
one = Pluto
two = 2
three = 3
eleven = 11

# Get all the initials, sort them alphabetically, and put them in a space-separated string.
' '.join(sorted(planet_to_initial.values()))

'E J M M N S U V'


for planet, initial in planet_to_initial.items():
    print("{} begins with \"{}\"".format(planet.rjust(10), initial))
   Mercury begins with "M"
     Venus begins with "V"
     Earth begins with "E"
      Mars begins with "M"
   Jupiter begins with "J"
    Saturn begins with "S"
    Uranus begins with "U"
   Neptune begins with "N"

planet.rjust(10) right-aligns the planet names to a width of 10 characters. 
If the name is shorter than 10 characters, it will be padded with spaces on the left.


enumerate() is a built-in Python function used to loop over an iterable object (such as a list, tuple, or string) 
while also tracking the index of the current item being processed. 
It returns an iterator that produces tuples containing the index and the corresponding item from the iterable.


enumerate(iterable, start=0)

my_list = ['apple', 'banana', 'cherry']
for index, fruit in enumerate(my_list):
    print(index, fruit)
This will output:

Copy code
0 apple
1 banana
2 cherryenumerate(iterable, start=0)




In Python, rstrip() is a string method used to remove trailing whitespace (spaces, tabs, newlines) from the right side of a string. It returns a new string with the whitespace removed.

Here's the basic syntax:

python
Copy code
string.rstrip([chars])





