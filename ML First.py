import pandas as pd

melbourne_file_path = 'C:\Users\pkaru\OneDrive\Desktop\business-operations-survey-2022-business-finance.csv'
melbourne_data = pd.read_csv(melbourne_file_path) 
melbourne_data.columns



y = melbourne_data.Price


#Choosing "Features"¶


The columns that are inputted into our model (and later used to make predictions) are called "features."

melbourne_features = ['Rooms', 'Bathroom', 'Landsize', 'Lattitude', 'Longtitude']
By convention, this data is called X.

X = melbourne_data[melbourne_features]

#eview the data we'll be using to predict house prices using the describe method and the head method, which shows the top few rows.

X.describe()

X.head()




Building Your Model
You will use the scikit-learn library to create your models. When coding, this library is written as sklearn, as you will see in the sample code. Scikit-learn is easily the most popular library for modeling the types of data typically stored in DataFrames.

The steps to building and using a model are:

Define: What type of model will it be? A decision tree? Some other type of model? Some other parameters of the model type are specified too.
Fit: Capture patterns from provided data. This is the heart of modeling.
Predict: Just what it sounds like
Evaluate: Determine how accurate the model's predictions are.




from sklearn.tree import DecisionTreeRegressor

# Define model. Specify a number for random_state to ensure same results each run
melbourne_model = DecisionTreeRegressor(random_state=1)

# Fit model
melbourne_model.fit(X, y)



In practice, you'll want to make predictions for new houses coming on the market rather than the houses we already have prices for. 
But we'll make predictions for the first few rows of the training data to see how the predict function works.

print("Making predictions for the following 5 houses:")
print(X.head())
print("The predictions are")
print(melbourne_model.predict(X.head()))



